﻿using Helpers.HealthVault.Interfaces;
using Microsoft.Health;
using Microsoft.Health.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Helpers.HealthVault
{
    public class WebApplicationUtilitiesHelper : IWebApplicationUtilitiesHelper
    {
        public PersonInfo LoadPersonInfoFromCookie()
        {
            return WebApplicationUtilities.LoadPersonInfoFromCookie(System.Web.HttpContext.Current);
        }

        public void RedirectToLogin(string TargetQuery)
        {
            WebApplicationUtilities.RedirectToShellUrl(System.Web.HttpContext.Current, "AUTH", TargetQuery);
        }

        public void RedirectToLogOut()
        {
            WebApplicationUtilities.SignOut(System.Web.HttpContext.Current);
        }
    }
}
