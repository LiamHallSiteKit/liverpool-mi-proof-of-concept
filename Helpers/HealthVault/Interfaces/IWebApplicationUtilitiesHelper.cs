﻿using Microsoft.Health;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Helpers.HealthVault.Interfaces
{
    public interface IWebApplicationUtilitiesHelper
    {
        PersonInfo LoadPersonInfoFromCookie();
        void RedirectToLogin(string TargetQuery);
        void RedirectToLogOut();
    }
}
