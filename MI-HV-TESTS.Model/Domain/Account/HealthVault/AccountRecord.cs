﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MI.Model.Domain.Account.HealthVault
{
    public class AccountRecord
    {
        public Guid AccountRecordID { get; set; }
        public Guid AccountID { get; set; }
        public Guid RecordID { get; set; }
        public DateTime Created { get; set; }

        public virtual UserProfile UserProfile { get; set; }
    }
}
