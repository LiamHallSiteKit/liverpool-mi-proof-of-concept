﻿using MI.Model.Domain.Account.HealthVault;
using Microsoft.Health;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MI.Model.Domain.Account
{
    public class UserProfile
    {
        public int MIId { get; set; }
        public virtual AccountRecord AccountRecord { get; set; }
    }
}
