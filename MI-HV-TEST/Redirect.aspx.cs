﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Glucophage.Web
{
    public partial class Redirect : Microsoft.Health.Web.HealthServiceActionPage
    {
        //We don't want this page to require log on because when we sign out, 
        //we still want this page to read the WCPage_ActionSignOut key in the
        //Web.Config and redirect us to the proper page on sign out
        protected override bool LogOnRequired
        {
            get
            {
                return false;
            }
        }
    }
}