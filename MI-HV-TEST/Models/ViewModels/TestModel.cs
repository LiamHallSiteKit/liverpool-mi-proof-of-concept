﻿using Microsoft.Health;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MI.Web.Models.ViewModels
{
    public class TestModel
    {
        public string Search { get; set; }
        public string Message { get; set; }
        public PersonInfo PersonInfo { get; set; }
    }
}