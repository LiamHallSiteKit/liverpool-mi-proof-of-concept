﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Health;

namespace MI.Web.Models
{
    public class PersonViewModel
    {
        public PersonInfo PersonInfo{ get; set; }
        public string Exception { get; set; }
    }
}