﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using @Profile = MI.Model.Domain.Account.UserProfile;

namespace MI.Web.Models.UserProfile
{
    public class UserProfileViewModel
    {
        public Profile UserProfile { get; set; }
        public bool StoreHealthVault { get; set; }
    }
}