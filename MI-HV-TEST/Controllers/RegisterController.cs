﻿using MI.Model.Domain.Account;
using MI.Model.Domain.Account.HealthVault;
using MI.SQL.Repository.Mapper;
using MI.SQL.Repository.Mapper.Interfaces;
using MI.SQL.Repository.Repositories;
using MI.SQL.Repository.Repositories.Interfaces;
using MI.HVRepository;
using MI.HVRepository.Interfaces;
using MI.Web.Models.UserProfile;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MI.Web.Controllers
{
    public class RegisterController : Controller
    {
        IUserProfileRepository _user_profile_repository;

        public RegisterController()
        {
            _user_profile_repository = new UserProfileRepository();
        }

        public RegisterController(IUserProfileRepository userProfileRepository)
        {
            _user_profile_repository = userProfileRepository;
        }

        public ActionResult Index(Guid PersonId)
        {
            var model = _user_profile_repository.GetItemByPersonId(PersonId);

            return View(model);
        }
    }
}
