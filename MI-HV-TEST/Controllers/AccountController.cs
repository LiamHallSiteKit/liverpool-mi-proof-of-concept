﻿using Helpers.HealthVault;
using Helpers.HealthVault.Interfaces;
using MI.Web.Models;
using MI.SQL.Repository.Repositories;
using MI.SQL.Repository.Repositories.Interfaces;
using Microsoft.Health;
using Microsoft.Health.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MI.Model.Domain.Account;
using MI.SQL.Repository.Mapper.Interfaces;
using MI.SQL.Repository.Mapper;


namespace MI.Web.Controllers
{
    public class AccountController : Controller
    {
        IRecordRepository _record_repository;
        IAccountRepository _account_repository;
        IAccountRecordRepository _account_record_repository;
        IAccountRecordMapper _account_record_mapper;
        
        IUserProfileRepository _user_profile_repository;

        IWebApplicationUtilitiesHelper _web_application_utilities;

        public AccountController()
        {
            _record_repository = new RecordRepository();
            _account_repository = new AccountRepository();
            _account_record_repository = new AccountRecordRepository();
            _web_application_utilities = new WebApplicationUtilitiesHelper();
            _user_profile_repository = new UserProfileRepository();
            _account_record_mapper = new AccountRecordMapper();
        }

        public AccountController(IRecordRepository recordRepository,
            IAccountRepository accountRepository,
            IAccountRecordRepository accountRecordRepository,
            IWebApplicationUtilitiesHelper webApplicationUtilitiesHelper,
            IUserProfileRepository userProfileRepository,
            IAccountRecordMapper accountRecordMapper)
        {
            _record_repository = recordRepository;
            _account_repository = accountRepository;
            _account_record_repository = accountRecordRepository;
            _web_application_utilities = webApplicationUtilitiesHelper;
            _user_profile_repository = userProfileRepository;
            _account_record_mapper = accountRecordMapper;
        }

        private static String TargetQuery
        {
            get 
            {
                return "appid=" + ConfigurationManager.AppSettings["ApplicationId"];
            }
        }

        public ActionResult Login()
        {
            _web_application_utilities.RedirectToLogin(TargetQuery);
            return null;
        }

        public ActionResult AuthenticationSuccess(Guid? token)
        {
            PersonInfo personInfo = _web_application_utilities.LoadPersonInfoFromCookie();

            var recordExists = _record_repository.Exists(personInfo.SelectedRecord.Id);
            var accountExists = _account_repository.Exists(personInfo.PersonId);

            if (!recordExists)
            {
                var created = _record_repository.Add(personInfo);
            }

            if (!accountExists)
            {
                var created = _account_repository.Add(personInfo);
            }

            var accountRecord = _account_record_mapper.MapToDomain(personInfo);

            var userProfileExists = _user_profile_repository.Exists(personInfo.PersonId);

            if (!userProfileExists)
            {
                var userProfile = new UserProfile();
                userProfile.AccountRecord = accountRecord;
                _user_profile_repository.Add(userProfile);
            }

            return RedirectToAction("Index", "Register", new { PersonId = personInfo.PersonId });
        }

        public ActionResult Logout()
        {
            _web_application_utilities.RedirectToLogOut();
            return null;
        }

        public ActionResult Logoutsuccess()
        {
            return View();
        }
    }
}
