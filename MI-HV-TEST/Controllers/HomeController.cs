﻿using MI.HVRepository;
using MI.HVRepository.Interfaces;
using MI.Web.Models;
using Microsoft.Health;
using Microsoft.Health.PatientConnect;
using Microsoft.Health.Web;
using Microsoft.Health.Web.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using Fixtures.HealthVault;
using MI.SQL.Repository.Repositories.Interfaces;
using MI.Web.Models.ViewModels;
using MI.SQL.Repository.Repositories;

namespace MI.Web.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        IOfflinePersonInfoRepository _offline_person_info_repository;
        IUserProfileRepository _user_profile_repository;

        public HomeController()
        {
            _offline_person_info_repository = new OfflinePersonInfoRepository(new Guid(ConfigurationManager.AppSettings["ApplicationId"]));
            _user_profile_repository = new UserProfileRepository();
        }

        public HomeController(IOfflinePersonInfoRepository offlinePersonInfoRepository)
        {
            _offline_person_info_repository = offlinePersonInfoRepository;
        }
        
        public ActionResult Index()
        {
            PersonViewModel model = new PersonViewModel();

            try
            {
                model.PersonInfo = WebApplicationUtilities.LoadPersonInfoFromCookie(System.Web.HttpContext.Current);
                return View(model);
            }
            catch
            {
                return null;
            }
        }

        public ActionResult Test()
        {
            var model = new TestModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Test(string Search)
        {
            TestModel model = new TestModel();

            try
            {
                int id = int.Parse(Search);
                var profile = _user_profile_repository.GetItemById(id);
                model.PersonInfo = _offline_person_info_repository.GetOfflinePerson(profile.AccountRecord.AccountID);
                model.Message = null;
            }
            catch
            {
                model.Message = "No users found for the MI-ID: " + Search;
            }
            
            return View(model);
        }
    }
}
