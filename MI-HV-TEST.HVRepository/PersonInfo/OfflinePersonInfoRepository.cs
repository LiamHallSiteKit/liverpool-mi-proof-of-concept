﻿using MI.HVRepository.Interfaces;
using Microsoft.Health;
using Microsoft.Health.Web;
using Microsoft.Health.Web.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MI.HVRepository
{
    public class OfflinePersonInfoRepository : IOfflinePersonInfoRepository
    {
        private readonly Guid _applicationID;

        public OfflinePersonInfoRepository(Guid applicationID)
        {
            _applicationID = applicationID;
        }

        public PersonInfo GetOfflinePerson(Guid personID)
        {
            OfflineWebApplicationConnection offlineWebApplicationConnection = new OfflineWebApplicationConnection(personID);
            WebApplicationCredential cred = new WebApplicationCredential(_applicationID);

            OfflineWebApplicationConnection offlineconnection = new OfflineWebApplicationConnection(cred, personID);
            offlineconnection.Authenticate();

            return offlineconnection.GetPersonInfo();
        }
    }
}
