﻿using Microsoft.Health;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MI.HVRepository.Interfaces
{
    public interface IOfflinePersonInfoRepository
    {
        PersonInfo GetOfflinePerson(Guid personID);
    }
}
