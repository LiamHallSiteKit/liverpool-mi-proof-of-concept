﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fixtures.DomainModels;
using Rhino.Mocks;
using MI.Web.Controllers;
using Helpers.HealthVault.Interfaces;
using Fixtures.HealthVault;
using MI.SQL.Repository.Repositories.Interfaces;
using MI.HVRepository.Interfaces;
using System.Configuration;
using Microsoft.Health;
using System.Web.Mvc;
using MI.Model.Domain.Account;
using MI.SQL.Repository.Mapper.Interfaces;

namespace MI.Web.Tests
{
    [TestClass]
    public class When_Account_Controller_Is_Called
    {
        //Fixtures
        PersonInfoFixtures _personInfoFixtures;
        AccountFixtures _account_fixtures;
        AccountRecordFixtures _account_record_fixtures;
        RecordFixtures _record_fixtures;
        
        //Dependencies
        IOfflinePersonInfoRepository _offline_person_info_repository;
        IRecordRepository _record_repository;
        IAccountRepository _account_repository;
        IAccountRecordRepository _account_record_repository;
        IWebApplicationUtilitiesHelper _web_application_utilities;
        IUserProfileRepository _user_profile_repository;
        IAccountRecordMapper _account_record_mapper;

        HomeController _home_controller;

        AccountController _accountController;

        [TestInitialize]
        public void SetUp()
        {
            //Fixtures
            _personInfoFixtures = new PersonInfoFixtures();
            _account_fixtures = new AccountFixtures();
            _account_record_fixtures = new AccountRecordFixtures();
            _record_fixtures = new RecordFixtures();
            
            //Dependencies
            _record_repository = MockRepository.GenerateMock<IRecordRepository>();
            _account_repository = MockRepository.GenerateMock<IAccountRepository>();
            _account_record_repository = MockRepository.GenerateMock<IAccountRecordRepository>();
            _web_application_utilities = MockRepository.GenerateMock<IWebApplicationUtilitiesHelper>();
            _offline_person_info_repository = MockRepository.GenerateMock<IOfflinePersonInfoRepository>();
            _user_profile_repository = MockRepository.GenerateMock<IUserProfileRepository>();
            _account_record_mapper = MockRepository.GenerateMock<IAccountRecordMapper>();
            
            _home_controller = new HomeController(_offline_person_info_repository);

            _accountController = new AccountController(_record_repository, _account_repository,
            _account_record_repository, _web_application_utilities, _user_profile_repository, _account_record_mapper);
        }

        [TestMethod]
        public void When_Account_Controller_Is_Called_with_log_in()
        {
            //Arrange
            var targetQuery = "appid=" + ConfigurationManager.AppSettings["ApplicationId"];
            _web_application_utilities.Expect(i => i.RedirectToLogin(targetQuery));
            
            //Action
            var result = _accountController.Login();
            
            //Assert
            Assert.IsNull(result);
            _web_application_utilities.VerifyAllExpectations();
        }

        [TestMethod]
        public void When_Account_Controller_Is_Called_with_log_out()
        {
            //Arrange
            _web_application_utilities.Expect(i => i.RedirectToLogOut());

            //Action
            var result = _accountController.Logout();

            //Assert
            Assert.IsNull(result);
            _web_application_utilities.VerifyAllExpectations();
        }

        [TestMethod]
        public void When_Account_Controller_Is_Called_with_Authentication_Success_with_valid_person_info_not_logged_in_before()
        {
            //Arrange
            var personInfo = _personInfoFixtures.Get_Liam_Hall();
            var accountRecordFixture = _account_record_fixtures.Get_liam_hall_site_kit();

            _web_application_utilities.Expect(i => i.LoadPersonInfoFromCookie()).Return(personInfo);

            var personIDArg = Arg<PersonInfo>.Is.Anything;

            _record_repository.Expect(i => i.Exists(personInfo.PersonId)).Return(false);
            _account_repository.Expect(i => i.Exists(personInfo.PersonId)).Return(false);
            _user_profile_repository.Expect(i => i.Exists(personInfo.PersonId)).Return(false);

            _record_repository.Expect(i => i.Add(Arg<PersonInfo>.Matches(a => a.PersonId == personInfo.PersonId))).Return(true);
            _account_repository.Expect(i => i.Add(Arg<PersonInfo>.Matches(a => a.PersonId == personInfo.PersonId))).Return(true);

            _user_profile_repository.Expect(i => i.Add(Arg<UserProfile>.Matches(a => a.AccountRecord.AccountID == personInfo.PersonId))).Return(true);

            _account_record_mapper.Expect(i => i.MapToDomain(Arg<PersonInfo>.Matches(a => a.PersonId == personInfo.PersonId)))
                .Return(accountRecordFixture);

            _account_record_repository.Expect(i => i.GetItemByAccountID(personInfo.PersonId)).Return(accountRecordFixture);

            //Action
            var result = _accountController.AuthenticationSuccess(null) as ActionResult;
          
            //Assert
            _account_repository.VerifyAllExpectations();
            _record_repository.VerifyAllExpectations();
            _web_application_utilities.VerifyAllExpectations();
            _user_profile_repository.VerifyAllExpectations();
        }

        [TestMethod]
        public void When_Account_Controller_Is_Called_with_Authentication_Success_with_valid_person_info_logged_in_before()
        {
            //Arrange
            var personInfo = _personInfoFixtures.Get_Liam_Hall();
            var accountRecordFixture = _account_record_fixtures.Get_liam_hall_site_kit();

            _web_application_utilities.Expect(i => i.LoadPersonInfoFromCookie()).Return(personInfo);

            var personIDArg = Arg<PersonInfo>.Is.Anything;

            _record_repository.Expect(i => i.Exists(personInfo.PersonId)).Return(true);
            _account_repository.Expect(i => i.Exists(personInfo.PersonId)).Return(true);
            _user_profile_repository.Expect(i => i.Exists(personInfo.PersonId)).Return(true);

            _account_record_mapper.Expect(i => i.MapToDomain(Arg<PersonInfo>.Matches(a => a.PersonId == personInfo.PersonId)))
                .Return(accountRecordFixture);

            //Action
            var result = _accountController.AuthenticationSuccess(null) as ActionResult;

            //Assert
            _account_repository.VerifyAllExpectations();
            _record_repository.VerifyAllExpectations();
            _account_record_repository.VerifyAllExpectations();
            _web_application_utilities.VerifyAllExpectations();
        }
    }
}
