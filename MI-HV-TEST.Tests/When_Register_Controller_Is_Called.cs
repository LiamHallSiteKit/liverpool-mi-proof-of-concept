﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fixtures;
using Fixtures.HealthVault;
using MI.HVRepository.Interfaces;
using MI.HVRepository;
using System.Configuration;
using Rhino.Mocks;
using MI.SQL.Repository.Mapper.Interfaces;
using Fixtures.DomainModels;
using Microsoft.Health;
using MI.Web.Controllers;
using MI.SQL.Repository.Repositories.Interfaces;

namespace MI.Web.Tests
{
    [TestClass]
    public class When_Register_Controller_Is_Called
    {
        //Fixtures
        UserProfileFixtures _user_profile_fixtures;

        IUserProfileRepository _user_profile_repository;

        //Object to be tested
        RegisterController _register_controller;

        [TestInitialize]
        public void SetUp()
        {
            _user_profile_fixtures = new UserProfileFixtures();

            _user_profile_repository = MockRepository.GenerateMock<IUserProfileRepository>();

            _register_controller = new RegisterController(_user_profile_repository);
        }
        
        [TestMethod]
        public void When_Register_Controller_Is_Index_is_called_with_valid_guid()
        {
            var userProfile = _user_profile_fixtures.Get_liam_hall_site_kit();
            var input = userProfile.AccountRecord.AccountID;

            _user_profile_repository.Expect(i => i.GetItemByPersonId(input)).Return(userProfile);

            var result =  _register_controller.Index(input);

            _user_profile_repository.VerifyAllExpectations();
        }
    }
}
