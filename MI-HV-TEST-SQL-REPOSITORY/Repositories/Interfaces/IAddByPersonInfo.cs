﻿using Microsoft.Health;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MI.SQL.Repository.Repositories.Interfaces
{
    public interface IAddByPersonInfo
    {
        bool Add(PersonInfo personInfo);
    }
}
