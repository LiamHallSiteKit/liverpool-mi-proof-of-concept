﻿using MI.Model.Domain.Account.HealthVault;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MI.SQL.Repository.Repositories.Interfaces
{
    public interface IRecordRepository : IRepository<Record, Guid>, IAddByPersonInfo
    {

    }
}
