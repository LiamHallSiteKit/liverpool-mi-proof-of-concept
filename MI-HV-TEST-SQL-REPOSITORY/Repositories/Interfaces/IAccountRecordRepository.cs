﻿using MI.Model.Domain.Account.HealthVault;
using Microsoft.Health;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MI.SQL.Repository.Repositories.Interfaces
{
    public interface IAccountRecordRepository :  IRepository<AccountRecord, Guid>, IAddByPersonInfo
    {
        AccountRecord GetItemByRecordID(Guid id);
        AccountRecord GetItemByAccountID(Guid id);
    }
}
