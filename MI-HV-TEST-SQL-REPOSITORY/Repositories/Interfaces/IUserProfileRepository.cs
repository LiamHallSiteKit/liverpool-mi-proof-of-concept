﻿using MI.Model.Domain.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MI.SQL.Repository.Repositories.Interfaces
{
    public interface IUserProfileRepository : IRepository<UserProfile, int>, IAddByDomainObject<UserProfile>, IQueryByPersonId<UserProfile>
    {

    }
}
