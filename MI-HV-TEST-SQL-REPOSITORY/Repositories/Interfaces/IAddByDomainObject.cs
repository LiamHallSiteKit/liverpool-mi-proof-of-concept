﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MI.SQL.Repository.Repositories.Interfaces
{
    public interface IAddByDomainObject<T>
    {
        bool Add(T domainObject);
    }
}
