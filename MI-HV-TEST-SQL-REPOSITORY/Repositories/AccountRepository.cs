﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MI.SQL.Repository.Repositories.Interfaces;
using MI.SQL.Repository.Context;
using Microsoft.Health;
using MI.SQL.Repository.Mapper.Interfaces;
using MI.SQL.Repository.Mapper;
using MI.Model.Domain.Account.HealthVault;

namespace MI.SQL.Repository.Repositories
{
    public class AccountRepository : IAccountRepository
    {
        ISQLContext _context;
        IAccountMapper _account_mapper;

        public AccountRepository()
        {
            _context = new SQLContext();
            _account_mapper = new AccountMapper();
        }

        public AccountRepository(ISQLContext sqlContext, IAccountMapper accountMapper)
        {
            _context = sqlContext;
            _account_mapper = accountMapper;
        }

        public bool Add(PersonInfo personInfo)
        {
            var account = _account_mapper.MapToDomain(personInfo);
            _context.Account.Add(account);
            return _context.SaveChanges() > 0;
        }

        public Account GetItemById(Guid id)
        {
            return _context.Account.Where(i => i.AccountID == id).FirstOrDefault();
        }

        public bool Exists(Guid id)
        {
            return GetItemById(id) != null;
        }

        public bool Delete(Account itemToBeDeleted)
        {
            throw new NotImplementedException();
        }
    }
}
