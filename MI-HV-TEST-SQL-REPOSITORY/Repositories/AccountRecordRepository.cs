﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MI.SQL.Repository.Repositories.Interfaces;
using MI.SQL.Repository.Context;
using Microsoft.Health;
using MI.SQL.Repository.Mapper.Interfaces;
using MI.SQL.Repository.Mapper;
using MI.Model.Domain.Account.HealthVault;

namespace MI.SQL.Repository.Repositories
{
    public class AccountRecordRepository : IAccountRecordRepository
    {
        ISQLContext _context;
        IAccountRecordMapper _account_record_mapper;

        public AccountRecordRepository(ISQLContext context, IAccountRecordMapper accountRecordMapper)
        {
            _context = context;
            _account_record_mapper = accountRecordMapper;
        }

        public AccountRecordRepository()
        {
            _context = new SQLContext();
            _account_record_mapper = new AccountRecordMapper();
        }

        public bool Add(PersonInfo personInfo)
        {
            var accountRecord = _account_record_mapper.MapToDomain(personInfo);

            _context.AccountRecord.Add(accountRecord);

            return _context.SaveChanges() > 0;
        }

        public AccountRecord GetItemByRecordID(Guid id)
        {
            return _context.AccountRecord.Where(i => i.RecordID == id).FirstOrDefault();
        }

        public AccountRecord GetItemByAccountID(Guid id)
        {
            return _context.AccountRecord.Where(i => i.AccountID == id).FirstOrDefault();
        }

        public AccountRecord GetItemById(Guid id)
        {
            return _context.AccountRecord.Where(i => i.AccountRecordID == id).FirstOrDefault();
        }

        public bool Exists(Guid id)
        {
            return GetItemById(id) != null;
        }

        public bool Delete(AccountRecord itemToBeDeleted)
        {
            throw new NotImplementedException();
        }
    }
}
