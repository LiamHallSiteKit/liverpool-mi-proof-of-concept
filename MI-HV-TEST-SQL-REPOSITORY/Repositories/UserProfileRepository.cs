﻿using MI.Model.Domain.Account;
using MI.SQL.Repository.Context;
using MI.SQL.Repository.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MI.SQL.Repository.Repositories
{
    public class UserProfileRepository : IUserProfileRepository
    {
        ISQLContext _context;

        public UserProfileRepository()
        {
            _context = new SQLContext();
        }

        public UserProfileRepository(ISQLContext sqlContext)
        {
            _context = sqlContext;
        }

        public UserProfile GetItemById(int id)
        {
            var data =  _context.UserProfile.Where(i => i.MIId == id).FirstOrDefault();
            return data;
        }

        public bool Exists(int id)
        {
            var item = GetItemById(id);
            return  item != null;
        }

        public bool Add(UserProfile domainObject)
        {
            _context.UserProfile.Add(domainObject);
            return _context.SaveChanges() > 0;
        }

        public bool Delete(UserProfile itemToBeDeleted)
        {
            var record = _context.Record.Where(i => i.RecordID == itemToBeDeleted.AccountRecord.RecordID).FirstOrDefault();
            if (record != null)
            {
                _context.Record.Remove(record);
            }

            var account = _context.Account.Where(i => i.AccountID == itemToBeDeleted.AccountRecord.AccountID).FirstOrDefault();
            if (account != null)
            {
                _context.Account.Remove(account);
            }

            _context.AccountRecord.Remove(itemToBeDeleted.AccountRecord);

            _context.UserProfile.Remove(itemToBeDeleted);
           
            return _context.SaveChanges() > 1;
        }

        public bool Exists(Guid personId)
        {
            return GetItemByPersonId(personId) != null;
        }

        public UserProfile GetItemByPersonId(Guid personId)
        {
            var data = _context.UserProfile.Where(i => i.AccountRecord.AccountID == personId).FirstOrDefault();
            
            if (data != null)
            {
                data.AccountRecord = _context.AccountRecord.Where(i => i.AccountID == personId).FirstOrDefault();
            }

            return data;
        }
    }
}
