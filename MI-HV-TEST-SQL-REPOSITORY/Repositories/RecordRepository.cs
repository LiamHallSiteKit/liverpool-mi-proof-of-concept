﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MI.SQL.Repository.Repositories.Interfaces;
using MI.SQL.Repository.Context;
using MI.SQL.Repository.Mapper.Interfaces;
using MI.SQL.Repository.Mapper;
using Microsoft.Health;
using MI.Model.Domain.Account.HealthVault;

namespace MI.SQL.Repository.Repositories
{
    public class RecordRepository : IRecordRepository
    {
        ISQLContext _context;
        IRecordMapper _record_mapper;

        public RecordRepository()
        {
            _context = new SQLContext();
            _record_mapper = new RecordMapper();
        }

        public RecordRepository(ISQLContext sqlContext, IRecordMapper recordMapper)
        {
            _context = sqlContext;
            _record_mapper = recordMapper;
        }

        public bool Add(PersonInfo personInfo)
        {
            var record = _record_mapper.MapToDomain(personInfo);

            _context.Record.Add(record);
            
            return _context.SaveChanges() > 0;
        }

        public Record GetItemById(Guid id)
        {
            return _context.Record.Where(i => i.RecordID == id).FirstOrDefault();
        }

        public bool Exists(Guid id)
        {
            return GetItemById(id) != null;
        }

        public bool Delete(Record itemToBeDeleted)
        {
            throw new NotImplementedException();
        }
    }
}
