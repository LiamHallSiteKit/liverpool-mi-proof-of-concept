﻿using MI.Model.Domain.Account.HealthVault;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MI.SQL.Repository.Mappers.SQLMap
{
    public class AccountRecordMap  : EntityTypeConfiguration<AccountRecord>
    {
        public AccountRecordMap()
        {
            ToTable("AccountRecord");

            HasKey(i => i.AccountRecordID);

            Property(p => p.AccountID)
                .HasColumnName("AccountID");
            
            Property(p => p.RecordID)
              .HasColumnName("RecordID");

            Property(p => p.Created)
              .HasColumnName("Created");
        }
    }
}
