﻿using MI.Model.Domain.Account;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MI.SQL.Repository.Mappers.SQLMap
{
    public class UserProfileMap : EntityTypeConfiguration<UserProfile>
    {
        public UserProfileMap()
        {
            ToTable("UserProfile");

            HasKey(t => t.MIId);
            Property(p => p.MIId)
                .HasColumnName("MIId")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            HasRequired(p => p.AccountRecord)
                .WithRequiredPrincipal(c => c.UserProfile)
                .WillCascadeOnDelete(false);
        }
    }
}
