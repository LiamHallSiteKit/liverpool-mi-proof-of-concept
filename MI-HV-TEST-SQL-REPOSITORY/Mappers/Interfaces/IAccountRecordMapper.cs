﻿using MI.Model.Domain.Account.HealthVault;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MI.SQL.Repository.Mapper.Interfaces
{
    public interface IAccountRecordMapper : IMapper<AccountRecord>
    {

    }
}
