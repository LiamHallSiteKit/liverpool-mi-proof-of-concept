﻿using MI.Model.Domain.Account.HealthVault;
using MI.SQL.Repository.Mapper.Interfaces;
using Microsoft.Health;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MI.SQL.Repository.Mapper
{
    public class RecordMapper : IRecordMapper
    {
        public Record MapToDomain(PersonInfo personInfo)
        {
            return new Record
            {
                RecordID = personInfo.SelectedRecord.Id,
                Created = DateTime.UtcNow
            };
        }
    }
}
