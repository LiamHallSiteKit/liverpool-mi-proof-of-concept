﻿using MI.Model.Domain.Account.HealthVault;
using MI.SQL.Repository.Mapper.Interfaces;
using Microsoft.Health;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MI.SQL.Repository.Mapper
{
    public class AccountRecordMapper : IAccountRecordMapper
    {
        public AccountRecord MapToDomain(PersonInfo personInfo)
        {
            return new AccountRecord
            {
                AccountRecordID = Guid.NewGuid(),
                AccountID = personInfo.PersonId,
                RecordID = personInfo.SelectedRecord.Id,
                Created = DateTime.UtcNow
            };
        }
    }
}
