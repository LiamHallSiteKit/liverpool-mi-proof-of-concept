﻿using MI.Model.Domain.Account.HealthVault;
using MI.SQL.Repository.Mapper.Interfaces;
using Microsoft.Health;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MI.SQL.Repository.Mapper
{
    public class AccountMapper : IAccountMapper
    {
        public Account MapToDomain(PersonInfo personInfo)
        {
            return new Account
            {
                AccountID = personInfo.PersonId,
                Created = DateTime.UtcNow
            };
        }
    }
}
