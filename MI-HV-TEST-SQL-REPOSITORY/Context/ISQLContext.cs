﻿using MI.Model.Domain.Account;
using MI.Model.Domain.Account.HealthVault;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MI.SQL.Repository.Context
{
    public interface ISQLContext
    {
        IDbSet<UserProfile> UserProfile { get; set; }
        IDbSet<Account> Account { get; set; }
        IDbSet<AccountRecord> AccountRecord { get; set; }
        IDbSet<Record> Record { get; set; }
        int SaveChanges();
    }
}
