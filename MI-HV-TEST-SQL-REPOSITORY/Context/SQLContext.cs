﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using MI.SQL.Repository.Context;
using MI.SQL.Repository;
using MI.Model.Domain;
using MI.Model.Domain.Account.HealthVault;
using MI.Model.Domain.Account;
using MI.SQL.Repository.Mappers.SQLMap;

namespace MI.SQL.Repository.Context
{
    public class SQLContext : DbContext, ISQLContext
    {
        public SQLContext()
            : base()
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserProfile>()
                .HasKey(t => t.MIId);

            modelBuilder.Entity<UserProfile>()
            .HasRequired(t => t.AccountRecord);
            base.OnModelCreating(modelBuilder);
        }

        public IDbSet<Account> Account { get; set; }
        public IDbSet<AccountRecord> AccountRecord { get; set; }
        public IDbSet<Record> Record { get; set; }
        public IDbSet<UserProfile> UserProfile { get; set; }
    }
}
