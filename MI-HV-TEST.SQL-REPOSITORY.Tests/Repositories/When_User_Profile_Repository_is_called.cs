﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fixtures;
using CustomAsserts;
using Fixtures.DomainModels;
using MI.SQL.Repository.Repositories;

namespace MI.SQL.Repository.Tests.Repositories
{
    [TestClass]
    public class When_User_Profile_Repository_is_called
    {
        UserProfileFixtures _user_profile_fixtures;
        UserProfileRepository _user_profile_repository;

        [TestInitialize]
        public void SetUp()
        {
            _user_profile_fixtures = new UserProfileFixtures();
            _user_profile_repository = new UserProfileRepository();
        }

        [TestMethod]
        public void When_User_Profile_Repository_add_and_delete_user()
        {
            var input = _user_profile_fixtures.Get_liam_hall_site_kit();

            if (_user_profile_repository.Exists(input.MIId))
            {
                _user_profile_repository.Delete(input);
            }
            
            var result = _user_profile_repository.Add(input);

            if (_user_profile_repository.Exists(input.MIId))
            {
                _user_profile_repository.Delete(input);
            }

            Assert.IsTrue(result);
        }
    }
}
