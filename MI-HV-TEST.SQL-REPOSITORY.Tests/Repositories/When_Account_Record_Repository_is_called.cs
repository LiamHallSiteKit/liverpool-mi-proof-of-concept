﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fixtures.DomainModels;
using Fixtures.DomainModels.Interfaces;
using MI.SQL.Repository.Repositories;
using MI.SQL.Repository.Context;
using Rhino.Mocks;
using System.Data.Entity;
using System.Collections.Generic;
using MI.SQL.Repository;
using MI.SQL.Repository.Repositories.Interfaces;
using MI.SQL.Repository.Mapper.Interfaces;



namespace MI.SQL.Repository.Tests.Repositories
{
    [TestClass]
    public class When_Account_Record_Repository_is_called
    {
        //Fixtures
        IAccountRecordFixtures _account_record_fixtures;

        //Dependencies
        ISQLContext _context;

        //Object to be tested
        IAccountRecordRepository _account_record_repoistory;
        IAccountRecordMapper _account_record_mapper;

        [TestInitialize]
        public void SetUp()
        {
            _account_record_fixtures = new AccountRecordFixtures();
            _context = MockRepository.GenerateMock<ISQLContext>();

            _account_record_mapper = MockRepository.GenerateMock<IAccountRecordMapper>();
            _account_record_repoistory = new AccountRecordRepository(_context, _account_record_mapper);
        }

        [TestMethod]
        public void When_Account_Record_Repository_get_is_called_with_valid_id()
        {
            Assert.Fail();
        }
    }
}
