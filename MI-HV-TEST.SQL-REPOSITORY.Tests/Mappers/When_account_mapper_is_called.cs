﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MI.SQL.Repository.Mapper;
using Fixtures.DomainModels;
using Fixtures.HealthVault;
using CustomAsserts;

namespace MI.SQL.Repository.Tests.Mappers
{
    [TestClass]
    public class When_account_mapper_is_called
    {
        //Fixtures
        AccountFixtures _account_fixtures;
        PersonInfoFixtures _person_info_fixtures;

        AccountMapper _account_mapper;

        [TestInitialize]
        public void SetUp()
        {
            _account_fixtures = new AccountFixtures();
            _person_info_fixtures = new PersonInfoFixtures();
            _account_mapper = new AccountMapper();
        }

        [TestMethod]
        public void When_account_mapper_is_called_with_valid_person_info()
        {
           var input = _person_info_fixtures.Get_Liam_Hall();
           var expected = _account_fixtures.Get_liam_hall_site_kit();

           var result = _account_mapper.MapToDomain(input);

           AccountAssert.AreEqual(expected, result);
        }
    }
}
