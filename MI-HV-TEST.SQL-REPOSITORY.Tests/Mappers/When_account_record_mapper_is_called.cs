﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MI.SQL.Repository.Mapper;
using Fixtures.DomainModels;
using Fixtures.HealthVault;
using CustomAsserts;


namespace MI.SQL.Repository.Tests.Mappers
{
    [TestClass]
    public class When_account_record_mapper_is_called
    {
        //Fixtures
        AccountRecordFixtures _account_record_fixtures;
        PersonInfoFixtures _person_info_fixtures;

        AccountRecordMapper _account_record_mapper;

        [TestInitialize]
        public void SetUp()
        {
            _account_record_fixtures = new AccountRecordFixtures();
            _person_info_fixtures = new PersonInfoFixtures();
            _account_record_mapper = new AccountRecordMapper();
        }

        [TestMethod]
        public void When_account_record_mapper_is_called_with_valid_person_info()
        {
            var input = _person_info_fixtures.Get_Liam_Hall();
            var expected = _account_record_fixtures.Get_liam_hall_site_kit();

            var result = _account_record_mapper.MapToDomain(input);

            AccountRecordAssert.AreEqual(expected, result);
        }
    }
}
