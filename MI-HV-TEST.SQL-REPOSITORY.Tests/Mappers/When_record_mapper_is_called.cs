﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MI.SQL.Repository.Mapper;
using Fixtures.DomainModels;
using Fixtures.HealthVault;
using CustomAsserts;


namespace MI.SQL.Repository.Tests.Mappers
{
    [TestClass]
    public class When_record_mapper_is_called
    {
        //Fixtures
        RecordFixtures _record_fixtures;
        PersonInfoFixtures _person_info_fixtures;
        
        RecordMapper _record_mapper;

        [TestInitialize]
        public void SetUp()
        {
            _record_fixtures = new RecordFixtures();
            _person_info_fixtures = new PersonInfoFixtures();
            _record_mapper = new RecordMapper();
        }

        [TestMethod]
        public void When_record_mapper_is_called_with_valid_person_info()
        {
            var input = _person_info_fixtures.Get_Liam_Hall();
            var expected = _record_fixtures.Get_liam_hall_site_kit();

            var result = _record_mapper.MapToDomain(input);

            RecordAssert.AreEqual(expected, result);
        }
    }
}
