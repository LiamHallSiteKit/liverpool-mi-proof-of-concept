﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fixtures;
using Fixtures.HealthVault;
using Microsoft.Health;
using CustomAsserts.HealthVault;

namespace MI_HV_TEST.HVRepository.Test
{
    [TestClass]
    public class When_offline_person_info_repository_is_called
    {
        PersonInfoFixture _person_info_fixtures;
        OfflinePersonInfoRepository _offline_person_repository;

        [TestInitialize]
        public void SetUp()
        {
            _person_info_fixtures = new PersonInfoFixture();
            var appId = _person_info_fixtures.Get_Liam_Hall().ApplicationConnection.ApplicationId;
            _offline_person_repository = new OfflinePersonInfoRepository(appId);
        }

        [TestMethod]
        public void When_offline_person_info_repository_is_called_with_valid_id()
        {
            var expected = _person_info_fixtures.Get_Liam_Hall();
            var input = expected.PersonId;

            var result = _offline_person_repository.GetOfflinePerson(input);

            PersonInfoAssert.AreEqual(expected, result);
        }
    }
}
