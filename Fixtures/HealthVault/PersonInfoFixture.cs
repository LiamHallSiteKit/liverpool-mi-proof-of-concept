﻿using MI.Model.Domain;
using MI.HVRepository;
using Microsoft.Health;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.XmlConfiguration;
using System.Xml.XPath;
using Rhino.Mocks;

namespace Fixtures.HealthVault
{
    public class PersonInfoFixtures
    {
        string _liam_hall_sitekit_path = "HealthVault/XML/LiamHall.xml";
        Guid _app_Id = new Guid("63c757ea-40fa-4e29-8f16-9dcf833a753f");
        string _app_name = "MI";
        string _app_url = "https://platform.healthvault-ppe.co.uk/platform";
        string _shell_url = "https://account.healthvault-ppe.co.uk";

        public PersonInfo Get_Liam_Hall()
        {
            var hs = new HealthServiceInstance(_app_Id.ToString(), _app_name, "", new Uri(_app_url), new Uri(_shell_url));
            var app = new ApplicationConnection(_app_Id, hs);
            var doc = new XPathDocument(_liam_hall_sitekit_path);
     
            var navigator = doc.CreateNavigator();
            navigator = navigator.SelectSingleNode("person-info").CreateNavigator();

            return PersonInfo.CreateFromXml(app, navigator) as PersonInfo;
        }
    }
}
