﻿using Fixtures.DomainModels.Interfaces;
using MI.Model.Domain.Account.HealthVault;
using MI.SQL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fixtures.DomainModels
{
    public class AccountRecordFixtures : IAccountRecordFixtures
    {
        public AccountRecord Get_liam_hall_site_kit()
        {
            return new AccountRecord
            {
                AccountRecordID = new Guid("4edb1a4f-7b40-4b04-bec3-a301bed93f59"),
                RecordID = new Guid("cae6456f-8440-41e0-8065-6749a411ac9b"),
                AccountID = new Guid("a5a2d331-7a7a-403e-888f-cc27942669f2"),
                Created = new DateTime(2014, 3, 14, 9, 0, 0, 0)
            };
        }

        public List<AccountRecord> Get_list_of_users()
        {
            return new List<AccountRecord>()
            {
                Get_liam_hall_site_kit()
            };
        }

        public List<AccountRecord> Get_empty_list_of_users()
        {
            return new List<AccountRecord>()
            {
     
            };
        }
    }
}
