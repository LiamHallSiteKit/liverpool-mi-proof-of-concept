﻿using Fixtures.DomainModels.Interfaces;
using MI.Model.Domain.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fixtures.DomainModels
{
    public class UserProfileFixtures : IUserProfileFixtures
    {
        AccountRecordFixtures _account_record_Fixtures = new AccountRecordFixtures();

        public UserProfile Get_liam_hall_site_kit()
        {
            return new UserProfile
            {
                MIId = int.MaxValue,
                AccountRecord = _account_record_Fixtures.Get_liam_hall_site_kit()
            };

            throw new NotImplementedException();
        }

        public List<UserProfile> Get_list_of_users()
        {
            return new List<UserProfile>()
            {
                Get_liam_hall_site_kit()
            };
        }

        public List<UserProfile> Get_empty_list_of_users()
        {
            return new List<UserProfile>();
        }
    }
}
