﻿using Fixtures.DomainModels.Interfaces;
using MI.Model.Domain.Account.HealthVault;
using MI.SQL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fixtures.DomainModels
{
    public class AccountFixtures : IAccountFixtures
    {
        public Account Get_liam_hall_site_kit()
        {
            return new Account
            {
                AccountID = new Guid("a5a2d331-7a7a-403e-888f-cc27942669f2"),
                Created = new DateTime(2014, 3, 14, 9,0,0,0)
            };
        }

        public List<Account> Get_list_of_users()
        {
            return new List<Account>()
            {
                Get_liam_hall_site_kit()
            };
        }

        public List<Account> Get_empty_list_of_users()
        {
            return new List<Account>()
            {

            };
        }
    }
}
