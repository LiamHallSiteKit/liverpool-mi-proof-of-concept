﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fixtures.DomainModels.Interfaces;
using MI.Model.Domain.Account.HealthVault;

namespace Fixtures.DomainModels
{
    public class RecordFixtures : IRecordFixtures
    {
        public Record Get_liam_hall_site_kit()
        {
            return new Record
            {
                RecordID = new Guid("cae6456f-8440-41e0-8065-6749a411ac9b"),
                Created = new DateTime(2014, 3, 14, 9, 0, 0, 0)
            };
        }

        public List<Record> Get_list_of_users()
        {
            return new List<Record>()
            {
                Get_liam_hall_site_kit()
            };
        }

        public List<Record> Get_empty_list_of_users()
        {
            return new List<Record>()
            {

            };
        }
    }
}
