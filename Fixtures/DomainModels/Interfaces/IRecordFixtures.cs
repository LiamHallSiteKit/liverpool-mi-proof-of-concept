﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MI.Model.Domain.Account.HealthVault;

namespace Fixtures.DomainModels.Interfaces
{
    public interface IRecordFixtures : ISQLFixture<Record>
    {

    }
}
