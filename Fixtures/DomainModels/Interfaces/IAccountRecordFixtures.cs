﻿using MI.Model.Domain.Account.HealthVault;
using MI.SQL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fixtures.DomainModels.Interfaces
{
    public interface IAccountRecordFixtures : ISQLFixture<AccountRecord>
    {

    }
}
