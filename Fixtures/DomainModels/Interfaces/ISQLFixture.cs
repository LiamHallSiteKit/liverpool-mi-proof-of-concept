﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fixtures.DomainModels.Interfaces
{
    public interface ISQLFixture<T>
    {
        T Get_liam_hall_site_kit();
        List<T> Get_list_of_users();
        List<T> Get_empty_list_of_users();
    }
}
