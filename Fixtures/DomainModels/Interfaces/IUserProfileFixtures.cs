﻿using MI.Model.Domain.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fixtures.DomainModels.Interfaces
{
    public interface IUserProfileFixtures : ISQLFixture<UserProfile>
    {

    }
}
