﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MI.SQL.Repository;
using MI.Model.Domain.Account.HealthVault;

namespace CustomAsserts
{
    public static class AccountRecordAssert
    {
        public static void AreEqual(AccountRecord expected, AccountRecord actual)
        {
            Assert.AreEqual(expected.AccountID, actual.AccountID);
            Assert.AreEqual(expected.RecordID, actual.RecordID);
            Assert.IsNotNull(actual.Created);
            Assert.IsNotNull(actual.AccountRecordID);
        }
    }
}
