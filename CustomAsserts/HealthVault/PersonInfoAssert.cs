﻿using System;
using Microsoft.Health;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace CustomAsserts.HealthVault
{
    public static class PersonInfoAssert
    {
        public static void AreEqual(PersonInfo expected, PersonInfo actual)
        {
            Assert.AreEqual(expected.PersonId, actual.PersonId);
            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.SelectedRecord.Id, actual.SelectedRecord.Id);
        }
    }
}
