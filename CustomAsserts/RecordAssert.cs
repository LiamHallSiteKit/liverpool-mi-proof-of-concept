﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MI.SQL.Repository;
using MI.Model.Domain.Account.HealthVault;

namespace CustomAsserts
{
    public static class RecordAssert
    {
        public static void AreEqual(Record expected, Record actual)
        {
            Assert.AreEqual(expected.RecordID, actual.RecordID);
            Assert.IsNotNull(actual.Created);
        }
    }
}
