﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MI.SQL.Repository;
using MI.Model.Domain.Account.HealthVault;

namespace CustomAsserts
{
    public static class AccountAssert
    {
        public static void AreEqual(Account expected, Account actual)
        {
            Assert.AreEqual(expected.AccountID, actual.AccountID);
            Assert.IsNotNull(actual.Created);
        }
    }
}
